  let picture = document.querySelectorAll('.image-to-show');
  let stopBtn = document.querySelector('.stop');
  let continueBtn = document.querySelector('.continue');

  function displayNone() {

      for (const item of picture) {
          item.style.display = "none";
          picture[0].style.display = "block";
      }

  }
  displayNone();


  let index = 0;

  function showPicture() {
      picture[index].style.display = "none";
      index++;
      if (index >= picture.length) {
          index = 0;
      }
      picture[index].style.display = "block";


  }
  let stopSet = setInterval(showPicture, 2000);
  stopBtn.addEventListener('click', function () {
      clearInterval(stopSet);
      stopSet = true;
  });

  continueBtn.addEventListener("click", () => {
      if (stopSet === true) {
          stopSet = setInterval(showPicture, 2000);
      }
  });